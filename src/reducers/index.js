import {combineReducers} from 'redux';
import selections from './selections';
import application from './application';

export default combineReducers({
    selections,
    application
})