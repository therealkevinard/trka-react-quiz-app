const initialState = [];

const selections = (state = initialState, action) => {
    switch (action.type) {
        case "SETSELECTION":
            let exists = state.filter(selection => {
                return selection.question === action.payload.question;
            }).length > 0;
            if (exists) {
                return state.map(selection => {
                    return selection.question === action.payload.question ? action.payload : selection
                })
            }
            else {
                return [
                    ...state,
                    action.payload
                ]

            }
        case "SELECTIONS.RESET":
            return [];
        default:
            return state;
    }
}

export default selections