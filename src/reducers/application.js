const initialState = {
    submitted: false,
    calculated: {},
};

const application = (state = initialState, action) => {
    switch (action.type) {
        case "SETSUBMIT":
            // expects action: {type:SETSUBMIT, submitted:boolean}
            return Object.assign({}, state, {submitted: action.submitted});
        case "SETCALCULATION":
            //
            return Object.assign({}, state, {calculated: action.payload})
        case "APP.RESET":
            return initialState;
        default:
            return state;
    }
}
export default application