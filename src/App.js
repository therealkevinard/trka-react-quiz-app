import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux'
import {setCalculation, setSubmit} from './actions/application'
import QuizQuestion from './components/QuizQuestion';
import FbShare from './components/FbShare';

const QuestionsModel = require('./model/questions');
const ResultsModel = require('./model/elves');

class App extends Component {
    constructor(props) {
        super(props);
        this.appContainer = React.createRef();
        this.questions = QuestionsModel;
        this.elves = ResultsModel;

        this.resetApp = props.resetApp.bind(this);
        this.resetSelections = props.resetSelections.bind(this);
        this.getFriendlyText = this.getFriendlyText.bind(this);

        this.calculate = this.calculate.bind(this)
    }

    reset() {
        this.resetApp()
        this.resetSelections();
        window.scrollTo(0, 0);
    }

    calculate() {
        if (this.props.selections.length === 0) {
            return;
        }

        let resultTallies = {};
        let calculated = [];

        let sum = this.props.selections.length;
        this.props.selections.map(sel => {
            resultTallies[sel.forResult]
                ? resultTallies[sel.forResult]++
                : resultTallies[sel.forResult] = 1;
        })

        Object.keys(resultTallies)
            .forEach(result => {
                let calc = {
                    result,
                    percent: Math.floor((resultTallies[result] / sum) * 100)
                };
                calc.result = this.elves.filter(resultNode => {
                    return resultNode.result === result;
                })[0];
                calculated.push(calc);
            });

        this.props.setCalculation(calculated);
        this.props.setSubmit(true);
        window.scrollTo(0, this.appContainer.current.offsetTop);
    }

    getFriendlyText() {
        return `I'm ${this.props.calculated[0].percent}% ${this.props.calculated[0].result.result}`
    }

    render() {
        return (
            <div className="App" ref={this.appContainer}>
                {!this.props.submitted &&
                <div className="container-fluid">
                    {this.questions.map((q, i) => {
                        return (
                            <QuizQuestion key={`question-${i}`}
                                          className={"row"}
                                          number={i + 1}
                                          text={q.question}
                                          answers={q.answers}/>
                        )
                    })}

                    <div className="row justify-content-around button-step-row">
                        <button
                            onClick={() => {
                                this.calculate()
                            }}
                            className="col-8 btn btn-outline-dark">Calculate
                        </button>
                    </div>

                </div>
                }

                {this.props.submitted &&
                <div className="container-fluid">
                    <div className="row justify-content-end button-step-row">
                        <FbShare app_id={562136417557677}
                                 redirect_uri="https://christmasfarminn.com/"
                                 href_base="https://christmasfarminn.com/white-mountain-attractions/new-hampshire-activities-and-accommodations-quiz/"
                                 quote={this.getFriendlyText()}
                        />
                        {/*<a className="btn btn-outline-dark" href={`https://www.facebook.com/dialog/share?app_id=562136417557677&display=page&redirect_uri=https://christmasfarminn.com/&href=https://christmasfarminn.com/white-mountain-attractions/new-hampshire-activities-and-accommodations-quiz/&quote=${this.getFriendlyText()}`}>Share your Results <i className="fa fa-facebook"></i></a>*/}
                    </div>
                    <div className="row results-row">
                        {this.props.calculated.map((calc, index) => {
                            return (
                                <div
                                    key={index}
                                    className={[
                                        "calculation col-12 calc-single",
                                        `calc-${calc.result.result.toLowerCase().replace(' ', '-')}`
                                    ].join(' ')}>
                                    <span className="percent">{calc.percent}%</span>
                                    <span className="calc-name">{calc.result.result}</span>
                                    <div className="calc-info" dangerouslySetInnerHTML={{__html: calc.result.bio}}></div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="row justify-content-around button-step-row">
                        <button
                            onClick={() => {
                                this.reset();
                            }}
                            className="col-8 btn btn-outline-dark">Start Over
                        </button>
                    </div>
                </div>
                }

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        submitted: state.application.submitted,
        selections: state.selections,
        calculated: state.application.calculated
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setSubmit: (submitStatus) => {
            return dispatch(setSubmit(submitStatus))
        },
        setCalculation: (calculation) => {
            return dispatch(setCalculation(calculation))
        },
        resetApp: () => {
            dispatch({type: "APP.RESET"});
        },
        resetSelections: () => {
            dispatch({type: "SELECTIONS.RESET"});
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
