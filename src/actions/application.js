export const setSubmit = (submitStatus = false) => {
    return {
        type: "SETSUBMIT",
        submitted: submitStatus
    }
}

export const setCalculation = (payload) => {
    return {
        type: "SETCALCULATION",
        payload
    }
}
