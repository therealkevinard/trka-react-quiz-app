import React from 'react';

const FbShare = ({app_id, redirect_uri, href_base, quote}) => {
    return(
        <a className="btn btn-outline-dark" href={`https://www.facebook.com/dialog/share?app_id=${app_id}&display=page&redirect_uri=${redirect_uri}&href=${href_base}&quote=${quote}`}>Share your Results <i className="fa fa-facebook"></i></a>
    )
}

export default FbShare