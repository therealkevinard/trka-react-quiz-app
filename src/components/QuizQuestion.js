import React, {Component} from 'react';
import {connect} from 'react-redux'
import QuizAnswer from "./QuizAnswer";
import {setSelection} from '../actions/selections';

class QuizQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: {}
        };

        this.onSelection = this.onSelection.bind(this);
    }

    onSelection = (selected) => {
        this.setState({selected});
        let payload = {
            question: parseInt(this.props.number),
            ...selected
        }
        this.props.setSelection(payload);
    }

    render() {
        return (
            <div className={[
                'question-single-outer',
                `question-single-outer-question-number-${this.props.number}`,
                this.props.className
            ].join(' ')}>
                <div className="question-single-banner col-12">
                    <span className="question-single-number">{this.props.number}.</span> <span className="question-single-question">{this.props.text}</span>
                </div>
                {this.props.answers.map((ans, i) => {
                    return (
                        <QuizAnswer
                            className="col-sm-12 col-md-6 col-xl-3"
                            key={`question-number-${this.props.number}-answer-${i}`}
                            onClick={(selectedAnswer) => {
                                this.onSelection(selectedAnswer);
                            }}
                            selected={this.state.selected === ans}
                            answer={ans}
                            forResult={ans.forResult}
                            text={ans.text}/>
                    )
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        setSelection: (selection) => {
            return dispatch(setSelection(selection))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuizQuestion)