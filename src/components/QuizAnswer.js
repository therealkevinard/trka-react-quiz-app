import React, {Component} from 'react';

class QuizAnswer extends Component {
    constructor(props) {
        super(props);
    }

    onSelectSelf = () => {
        this.props.onClick(this.props.answer);
    }

    render() {
        return (
            <div
                className={[
                    this.props.className,
                    this.props.selected ? "selected" : "not-selected",
                    "question-single-answer"
                ].join(" ")}
                onClick={() => {
                    this.onSelectSelf()
                }}>
                <div className="question-single-answer-inner">
                    <span>{this.props.answer.text}</span>
                </div>
            </div>
        )
    }
}

export default QuizAnswer;